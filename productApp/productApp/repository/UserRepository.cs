﻿using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    internal class UserRepository
    {

        User[] users;
        public UserRepository()
        {
            users = new User[1];
        }
        internal string Login(UserLoginInfo userLoginInfo)
        {
            if ((userLoginInfo.name == null || userLoginInfo.name == "") && (userLoginInfo.password == null || userLoginInfo.password == ""))
            {
                return "Fill Details";
            }
            else if (users.Length >= 0)
            {
                string loginResult = "";
                foreach (User user in users)
                {
                    if (user.name == userLoginInfo.name && user.password == userLoginInfo.password)
                    {
                        loginResult = "Login Success";
                    }
                }
                return loginResult;
            }
            else
            {
                return "Invalid Credentials!!!!!!";
            }
        }

        internal User[] GetAllUsers()
        {
            return users;
        }

        internal bool Register(User user)
        {
            users[0] = user;
            return true;
        }
    }
}