﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Models
{
    internal class User
    {
        internal int? id;
        internal string? name;
        internal string? password;
        internal string? email;
        internal string? firstName;
        internal string? city;

        public override string ToString()
        {
            return $"{id}\t{name}\t {email}\t{firstName} \t{city}";
        }
    }
}